import About from "./About/About";
import Education from "./Education/Education";
import Experience from "./Experience/Experience";
import Me from "./Me/Me";
import More from "./More/More";
import NavBar from "./NavBar/NavBar";
import LanguageSelector from "./LanguageSelector/LanguageSelector";
import Header from "./Header/Header";
import Footer from "./Footer/Footer";


export {About, Education, Experience, Me, More, NavBar, LanguageSelector, Header, Footer}